# DonjonHakimJulienGaëtan

- Trinome :
	- Benamara Hakim  
	- Dorne Julien  
	- Delacroix Gaëtan  
	
## Configuration

Créez une nouvelle variable d'environement nommé : "JUGAHA", avec le chemin.  


## Lancement

Lors du lancement, 3 choix s'offres a l'utilisateur

* 1 : Top 10
	* Permet d'afficher le top 10 des joueurs.  
	
* 2 : Nouvelle partie
	* Permet de lancer une nouvelle partie, taper la taille du donjon entre 2 et 23, puis taper le nom du joueur.
	* Le joueur démarre dans la salle d'entrée, tout en haut a gauche
	* Plusieurs choix s'offre au joueur :
		* Il peut observer la salle dans laquelle il se trouve, affichant toute les informations.  
		* Il peux observer les salle adjacente ou un acces est disponible.  
		* S'il y a un monstre, il doit le combattre et le vaincre avant de pouvoir utiliser un objet ou se déplacer.  
		* S'il y a des objets, il peux les utiliser. 4 objets sont disponible :
			* La potion de vie : donne de la vie au joueur.  
			* La potion de force : donne de la force au joueur.  
			* La bourse d'or : donne de l'or au joueur.  
			* Le bandit manchot : en echange d'or, utilise un objet aleatoire precedent plusieurs fois.  
		* Se déplacer vers une salle adjacente avec un point d'acces.  
	* Si les points de vie du joueur tombe a 0 ou moins, le joueur meurt et c'est la fin de la partie.
	* Si le joueur arrive dans la derniere salle, il devra combattre de boss du donjon, s'il gagne, le joueur a gagné et son score est enregistré, sinon se référer au point précédent.  
	
* 3 : Quitter
	* Permet de quitter le programme.  
	
## Fonctionnalité  

- [X] Generation de donjon aleatoire
- [X] Se déplacer dans le donjon
- [X] Utiliser des objets
- [X] Combattre un monstre
- [X] Afficher le top 10
- [X] Deplacement aleatoire des monstres
- [] Deplacement vers le joueur/ aleatoirement
