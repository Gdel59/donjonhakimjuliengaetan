package fr.afpa.GHJ.donjon.ihm;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import fr.afpa.GHJ.donjon.controles.ControleDonjon;
import fr.afpa.GHJ.donjon.entites.Donjon;
import fr.afpa.GHJ.donjon.entites.Joueur;
import fr.afpa.GHJ.donjon.entites.Salle;
import fr.afpa.GHJ.donjon.services.ServiceDonjon;
import fr.afpa.GHJ.donjon.services.ServiceGeneral;

public class AffichageDonjon {
	/**
	 * Affiche l'etat de toute les salles du donjon
	 * 
	 * @param donjon le donjon
	 */
	public static void affichageSalles(Donjon donjon) {

	}

	/**
	 * vide la console windows
	 */
	private static void clearScreen() {
		try {
			new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
		} catch (InterruptedException | IOException e) {
			System.out.println("erreur");
		}
	}
	
	public static void titre() {
		System.out.println(Salle.ROUGE +"                     _                 ,.,         ,.,                            __'                            ,.-:~:-.                ,:'/¯/`:,        .·/¯/`:,'                ,.-:~:-.            \r\n" + 
				"               .´/: : :/:`;        ,·'/:::/     '/:::/';     '            ,.·:'´::::::::`'·-.                 /':::::::::'`,            /:/_/::::/';    /:/_/::::';             /':::::::::'`,          \r\n" + 
				"              /:/:_: /:::'i       '/:/;:;/    ,/:;:/:::'`,             '/::::::::::::::::::';              /;:-·~·-:;':::',          /:'     '`:/::;  /·´    `·,::';          /;:-·~·-:;':::',         \r\n" + 
				"             /·´     '`;:::;'     /·´    ;`   '\\    '`;::::\\           /;:· '´ ¯¯  `' ·-:::/'            ,'´          '`:;::`,        ;         ';:';  ;         ';:;        ,'´          '`:;::`,       \r\n" + 
				"             i         'i::;     /      /.      \\     '`;:::',       /.'´      _         ';/'           /                `;::\\       |         'i::i  i         'i:';°      /                `;::\\      \r\n" + 
				"             ';        'i::;°  ,'      ;':';       '\\      \\::'i'°   ,:     ,:'´::;'`·.,_.·'´.,         ,'                   '`,::;     ';        ;'::/¯/;        ';:;'    ,'                   '`,::;    \r\n" + 
				"    ,. -.,   ';        ';::;   ;     'i::/`':.,_,.:'i      ;:/°   /     /':::::/;::::_::::::::;     i'       ,';´'`;         '\\:::',  'i        i':/_/:';        ;:';°   i'       ,';´'`;         '\\:::', \r\n" + 
				"   /:::::/`:.,;       ';::;'   i      ';/::::;::/::;'      i/'   ,'     ;':::::'/·´¯     ¯'`·;:::¦  ,'        ;' /´:`';         ';:::'i  ;       i·´   '`·;       ;:/°  ,'        ;' /´:`';         ';:::'i\r\n" + 
				"  /;:-:;/:::::'|       ;:/`;  ';      '`·-:;_;:·'´       ,' °  'i     ';::::::'\\             ';:';  ;        ;/:;::;:';         ',:::;  ';      ;·,  '  ,·;      ;/'    ;        ;/:;::;:';         ',:::;\r\n" + 
				",´      `·:;:·'       ;/'::/    ';                      ,'       ;      '`·:;:::::`'*;:'´      |/'  'i        '´        `'         'i::'/   ';    ';/ '`'*'´  ';    ';/' '  'i        '´        `'         'i::'/\r\n" + 
				"';                     `;/'      '\\                   ,·'         \\          '`*^*'´         /'   ¦       '/`' *^~-·'´\\         ';'/'    \\   /          '\\   '/'      ¦       '/`' *^~-·'´\\         ';'/'\r\n" + 
				"  '`·,           _,.-·'´ °        `·.,         ,. ·´              `·.,               ,.-·´      '`., .·´              `·.,_,.·´       '`'´             `''´   '    '`., .·´              `·.,_,.·´  \r\n" + 
				"       '`'*^*'´¯                       ¯`'´¯                         '`*^~·~^*'´                                                                    '                                           \r\n" + 
				""+Salle.BLANC);
	}

	/**
	 * permet de lancer le jeu
	 * 
	 * @param in le scanner
	 */
	public static void MenuNewGame(Scanner in) {
		while (true) {
			clearScreen();
			titre();
			System.out.println("												  1-Top 10");
			System.out.println("												2-Nouvelle Partie");
			System.out.println("												  3-Quitter");
			int choix = 0;
			try {
				choix = in.nextInt();
				in.nextLine();
				if (choix == 1) {
					List<String> top10 = ServiceGeneral.chargerDonnees();
					Collections.reverse(top10);
					for (int i = 1; i < 11; i++) {
						try {
							if (top10.get(i - 1) != null) {
								String[] res = top10.get(i - 1).split(";");
								System.out.println(i + "- " + res[0] + " ~ " + res[1]);
								try {
									TimeUnit.SECONDS.sleep(1);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} catch (NoSuchElementException e) {

						} catch (NullPointerException e) {

						} catch (IndexOutOfBoundsException e) {

						}
					}
					try {
						TimeUnit.SECONDS.sleep(3);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (choix == 2) {
					int x = -1;
					int y = -1;
					do {
						System.out.println("Veuillez Choisir les dimensions du labyrinthe (entre 2 et 23):");
						x = in.nextInt();
						in.nextLine();
						y = in.nextInt();
						in.nextLine();
					} while (!ControleDonjon.verifDimension(x, y));
					Donjon donjon = new ServiceDonjon(x, y).creationDonjon();

					System.out.println("Veuillez saisir votre nom : ");
					Joueur joueur = new Joueur(100, 20, 0, in.nextLine());

					donjon.getOneSalle(0, 0).setJoueur(joueur);
					Salle sortie = ServiceDonjon.rechercheSalleFinale(donjon);
					Salle actuelle = ServiceDonjon.rechercheSalleViaJoueur(donjon, joueur);
					
					
//					musique();
					
					
					Intro(joueur);
					while (joueur.getPv() > 0 && (sortie != actuelle || !sortie.getListeDeMonstre().isEmpty())) {
						clearScreen();
						ServiceDonjon.tourDeJeu(donjon, joueur, in);
						actuelle = ServiceDonjon.rechercheSalleViaJoueur(donjon, joueur);
					}
					clearScreen();
					if (joueur.getPv() <= 0)
						System.out.println(
								"   )\\.-.      /`-.    )\\   )\\   )\\.---.          .-./(       .-.   )\\.---.     /`-.  \r\n"
										+ " ,' ,-,_)   ,' _  \\  (  ',/ /  (   ,-._(       ,'     )  ,'  /  ) (   ,-._(  ,' _  \\ \r\n"
										+ "(  .   __  (  '-' (   )    (    \\  '-,        (  .-, (  (  ) | (   \\  '-,   (  '-' ( \r\n"
										+ " ) '._\\ _)  )   _  ) (  \\(\\ \\    ) ,-`         ) '._\\ )  ) './ /    ) ,-`    ) ,_ .' \r\n"
										+ "(  ,   (   (  ,' ) \\  `.) /  )  (  ``-.       (  ,   (  (  ,  (    (  ``-.  (  ' ) \\ \r\n"
										+ " )/'._.'    )/    )/      '.(    )..-.(        )/ ._.'   )/..'      )..-.(   )/   )/ \r\n"
										+ "                                                                                     ");
					else if (sortie == actuelle) {
						System.out.println(Salle.ROUGE+"     )      )                        (         )  \r\n"
								+ "  ( /(   ( /(              (  (      )\\ )   ( /(  \r\n"
								+ "  )\\())  )\\())      (      )\\))(   '(()/(   )\\()) \r\n"
								+ " ((_)\\  ((_)\\       )\\    ((_)()\\ )  /(_)) ((_)\\  \r\n"
								+ "__ ((_)   ((_)   _ ((_)   _(())\\_)()(_))    _((_) \r\n"
					+ Salle.BLANC+"\\ \\ / /  / _ \\  | | | |   \\ \\"+Salle.ROUGE+"((_)"+Salle.BLANC+"/ /|_ _|  | \\| | \r\n"
								+ " \\ V /  | (_) | | |_| |    \\ \\/\\/ /  | |   | .` | \r\n"
								+ "  |_|    \\___/   \\___/      \\_/\\_/  |___|  |_|\\_| \r\n"
								+ "                                                  ");
						ServiceGeneral.sauvegarderJoueur(joueur);
					}
					try {
						TimeUnit.SECONDS.sleep(5);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else if (choix == 3)
					break;
			} catch (InputMismatchException e) {
				try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				System.out.println("erreur de saisie");
				in.nextLine();
			}
			if (choix < 1 || choix > 3) {
				System.out.println("Veuillez rentrer un chiffre correspondant au menu");
			}

		}

	}

	private static void musique() {
		try {
	           AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File("missionimpossible.wav"));
	           Clip clip = AudioSystem.getClip();
	           clip.open(audioIn);
	           clip.start();
	       }
		catch (LineUnavailableException | UnsupportedAudioFileException | IOException e) {
		}
	}

	/**
	 * lance l'intro du donjon
	 */
	private static void Intro(Joueur joueur) {
		try {
			System.out.println("Bonjour jeune guerrier");
			TimeUnit.SECONDS.sleep(1);
			System.out.println("Bienvenu dans le merveilleux monde de \033[04mJAVA\033[0m");
			TimeUnit.SECONDS.sleep(1);
			System.out.println(
					"Afin de sauver notre monde menacé par la destruction, l'unique Rune capable de contrer ce danger à été dissimulée dans un donjon magique ");
			TimeUnit.SECONDS.sleep(2);
			System.out.println("\033[04mLE DONJON JUGAHA\033[0m");
			TimeUnit.SECONDS.sleep(2);
			System.out.println(
					"Ce donjon peuplé d'objets magiques est aussi un repere de monstres les plus puissants, seul les êtres courageux et pieux peuvent en sortir indemne");
			TimeUnit.SECONDS.sleep(3);
			System.out.println("Sauras tu relever le defi " + joueur.getNom() + " le monde JAVA a besoin de toi");
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			System.out.println("Erreur : interruption");
		}
	}

	/**
	 * permet d'afficher l'etat du donjon
	 * 
	 * @param donjon le donjon
	 */
	public static void affichageEtatDonjon(Donjon donjon) {
		for (int i = 0; i < donjon.getSalle().length; i++) {
			String[] res = new String[5];
			for (int j = 0; j < res.length; j++) {
				res[j] = "";
			}
			for (int j = 0; j < donjon.getSalle()[0].length; j++) {
				String[] all = donjon.getOneSalle(i, j).toString().split("\n");
				for (int k = 0; k < 5; k++) {
					res[k] += all[k];
				}
			}
			System.out.println(res[0]);
			System.out.println(res[1]);
			System.out.println(res[2]);
			System.out.println(res[3]);
			System.out.println(res[4]);
		}

	}
}