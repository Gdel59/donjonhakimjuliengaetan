package fr.afpa.GHJ.donjon.ihm;

import java.util.Scanner;

import fr.afpa.GHJ.donjon.entites.Donjon;
import fr.afpa.GHJ.donjon.entites.Joueur;
import fr.afpa.GHJ.donjon.entites.Salle;
import fr.afpa.GHJ.donjon.services.ServiceJoueur;

public class AffichageMenuJoueur {

	/**
	 * Méthode qui permet d'adapter l'affichage des actions proposées au joueur en
	 * fonction de la présence ou non d'objets/de monstres dans la salle
	 * 
	 * @param donjon Donjon
	 * @param salle  Salle
	 * @param in     Scanner
	 * @param joueur Joueur
	 */

	public static void affichageActions(Donjon donjon, Salle salle, Scanner in, Joueur joueur) {
		System.out.println("1 - Regarder");
		// Si il y a au moins un monstre dans la salle
		if (salle.getListeDeMonstre().size() > 0) {
			System.out.println("2 - Combattre");
		}

		// Si il n'y a pas de monstre et pas d'objet dans la salle
		if (salle.getListeDeMonstre().size() < 1 && salle.getListeObjet().size() < 1) {
			System.out.println("2 - Se déplacer");
		}

		// Si il n'y a pas de monstre et si il y a au moins un objet
		if (salle.getListeDeMonstre().size() < 1 && salle.getListeObjet().size() > 0) {

			System.out.println("2 - Se déplacer");
			for (int i = 3; i < salle.getListeObjet().size() + 3; i++) {
				System.out.println(i + " - " + "Utiliser " + salle.getListeObjet().get(i - 3).toString());
			}
		}

	}

	/**
	 * affiche le menu de direction pour choisir l'endroit ou regarder
	 * 
	 * @param salle la salle actuelle
	 */
	public static void affichageRegarder(Salle salle) {

		System.out.println("P - Regarder dans la Pièce");

		if (salle.isPointAccesNord())
			System.out.println("N - Regarder au Nord");

		if (salle.isPointAccesSud())
			System.out.println("S - Regarder au Sud");

		if (salle.isPointAccesEst())
			System.out.println("E - Regarder à l'Est");

		if (salle.isPointAccesOuest())
			System.out.println("O - Regarder à l'Ouest");

	}

	/**
	 * affiche le menu de direction pour choisir l'endoir ou se deplacer
	 * 
	 * @param salle la salle actuelle
	 */
	public static void affichageSeDeplacer(Salle salle) {

		if (salle.isPointAccesNord())
			System.out.println("N - Se déplacer vers le Nord");

		if (salle.isPointAccesSud())
			System.out.println("S - Se déplacer vers le Sud");

		if (salle.isPointAccesEst())
			System.out.println("E - Se déplacer vers l'Est");

		if (salle.isPointAccesOuest())
			System.out.println("O - Se déplacer vers l'Ouest");

	}

	/**
	 * Méthode permettant d'adapter l'affichage des actions proposées aux joueurs en
	 * fonction du nombre de monstre présent dans la salle
	 * 
	 * @param donjon Donjon
	 * @param salle  Salle
	 * @param in     Scanner
	 */
	public static void affichageChoixMonstres(Donjon donjon, Salle salle, Scanner in, Joueur joueur) {

		if (salle.getListeDeMonstre().size() > 0) {
			int choix = -1;
			while (!(choix > 0 && choix < salle.getListeDeMonstre().size()))
				for (int i = 1; i <= salle.getListeDeMonstre().size(); i++) {

					System.out.println(i + " - " + "Combattre " + salle.getListeDeMonstre().get(i).toString());

				}

			System.out.println();
			System.out.println("Veuillez choisir le monstre à combattre: ");
			choix = in.nextInt();
			choix = choix - 1;

			ServiceJoueur.combattre(donjon, joueur, salle, salle.getListeDeMonstre().get(choix));

		}

	}

}
