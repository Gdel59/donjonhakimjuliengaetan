package fr.afpa.GHJ.donjon.controles;

public class ControleDonjon {
	/**
	 * Verification des dimensions du donjon
	 * 
	 * @param x nombre de ligne
	 * @param y nombre de colonne
	 * @return true si les dimension sont correct, false si non
	 */
	public static boolean verifDimension(int x, int y) {
		if (x > 1 && y > 1 && x < 24 & y < 24) {
			return true;
		}
		System.out.println("saisir deux nombres entre 2 et 23");
		return false;
	}

	/**
	 * verifie si la valeur est entre 0 et le parametre
	 * 
	 * @param v     la valeur a verifier
	 * @param upper la valeur max
	 * @return true si la valeur est entre les deux, false si non
	 */
	public static boolean between(int v, int upper) {
		return (v >= 0) && (v < upper);
	}
}
