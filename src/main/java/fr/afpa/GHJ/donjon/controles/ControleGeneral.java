package fr.afpa.GHJ.donjon.controles;

public class ControleGeneral {
	/**
	 * verifie si le choix est correct
	 * 
	 * @param choix le char a verifier
	 * @return true si le choix est correct, false si non
	 */
	public static boolean verifSeDeplacer(char choix) {
		if (choix == 'N' || choix == 'S' || choix == 'E' || choix == 'O') {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * verifie si le choix est correct
	 * 
	 * @param choix le char a verifier
	 * @return true si le choix est correct, false si non
	 */
	public static boolean verifRegarder(char choix) {
		if (choix == 'N' || choix == 'S' || choix == 'E' || choix == 'O' || choix == 'P') {
			return true;
		} else {
			return false;
		}
	}
}
