package fr.afpa.GHJ.donjon.entites;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Donjon {
	// Attribut(s)
	private Salle[][] salle;

	// Constructeur(s)
	/**
	 * Constructeur normal de la classe Donjon
	 * 
	 * @param x le nombre de ligne
	 * @param y le nombre de colonne
	 */
	public Donjon(int x, int y) {
		this.salle = new Salle[x][y];
	}

	/**
	 * Constructeur par defaut
	 */
	public Donjon() {
		this.salle = new Salle[1][1];
	}

	// Getter(s)
	/**
	 * renvoi une salle du donjon
	 * 
	 * @param x la coordonnee x de la salle
	 * @param y la coordonnee y de la salle
	 * @return la salle choisit
	 */
	public Salle getOneSalle(int x, int y) {
		return this.salle[x][y];
	}

	/**
	 * modifie une salle du donjon
	 * 
	 * @param x     la coordonnee x de la salle
	 * @param y     la coordonnee y de la salle
	 * @param salle la salle choisit
	 */
	public void setOneSalle(Salle salle, int x, int y) {
		this.salle[x][y] = salle;
	}

	@Override
	public String toString() {
		String res = "";
		for (Salle[] salles : salle) {
			for (Salle salle : salles) {
				res += salle.toString();
			}
			res += "\n";
		}
		return res;
	}
}
