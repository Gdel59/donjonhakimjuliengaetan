package fr.afpa.GHJ.donjon.entites;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class PotionDeSoin implements IObjet {

	@Override
	public boolean effet(Joueur joueur) {
		int aleatoire = new Random().nextInt(30);
		joueur.setPv(joueur.getPv() + aleatoire);
		System.out.println("Vous avez obtenu " + aleatoire + " de vie !");
		System.out.println("Vos points de vie sont actuellement a " + joueur.getPv());
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;

	}

	@Override
	public String toString() {
		return "Potion de soin";
	}

}
