package fr.afpa.GHJ.donjon.entites;

public class EntreeDonjon extends Salle {

	public EntreeDonjon(Joueur joueur, boolean pointAccesNord, boolean pointAccesSud, boolean pointAccesEst,
			boolean pointAccesOuest) {
		super(joueur, pointAccesNord, pointAccesSud, pointAccesEst, pointAccesOuest);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "# # # # #\n" 
			 + "# " + (this.getJoueur() != null ? VERT + "P" : " ") + BLANC + "   " + ROUGE + "E" + BLANC + " #\n"
			 + "#       " + (this.isPointAccesEst() ? " \n" : "#\n")
			 + "# "+(this.getListeDeMonstre().size() > 0? BLEU +this.getListeDeMonstre().size()+ "M" : "  ")+BLANC+"    #\n"
			 + "# # " + (this.isPointAccesSud() ? " " : "#") + " # #\n";
	}

}
