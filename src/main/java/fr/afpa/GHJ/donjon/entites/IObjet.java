package fr.afpa.GHJ.donjon.entites;

public interface IObjet {
	/**
	 * Active l'effet de l'objet en question
	 * 
	 * @param joueur le joueur sur lequel l'effet se produit
	 * @return true si l'effet s'est activer, false si non
	 */
	public abstract boolean effet(Joueur joueur);

}