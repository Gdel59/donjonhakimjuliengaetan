package fr.afpa.GHJ.donjon.entites;

import java.util.ArrayList;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode

public class Salle {
	public final static String BLANC = "\033[0m";
	public final static String ROUGE = "\033[31m";
	public final static String VERT = "\033[32m";
	public final static String JAUNE = "\033[33m";
	public final static String BLEU = "\033[34m";
	public final static String MAGENTA = "\033[35m";
	public final static String CYAN = "\033[36m";
	
	
	protected List<Monstre> ListeDeMonstre;
	protected Joueur joueur;
	protected List<IObjet> ListeObjet;
	protected boolean PointAccesNord;
	protected boolean PointAccesSud;
	protected boolean PointAccesEst;
	protected boolean PointAccesOuest;

	@Override
	public String toString() {
		return "# # " + (this.isPointAccesNord() ? " " : "#") + " # #\n"
			 + "# "+(this.getJoueur() != null ? VERT + "P" : " ") + BLANC + "     #\n"
			 + (this.isPointAccesOuest() ? " " : "#") + "       " + (this.isPointAccesEst() ? " \n" : "#\n")
			 + "# "+(this.getListeDeMonstre().size() > 0? BLEU +this.getListeDeMonstre().size()+ "M" : "  ")+JAUNE+" " + (this.getListeObjet().size() > 0 ? this.getListeObjet().size() + "O" : "  ") +BLANC+ " #\n"
			 + "# # "+ (this.isPointAccesSud() ? " " : "#") + " # #\n";
	}

	public Salle(Joueur joueur, boolean pointAccesNord, boolean pointAccesSud, boolean pointAccesEst,
			boolean pointAccesOuest) {
		super();
		this.ListeDeMonstre = new ArrayList<Monstre>();
		this.joueur = joueur;
		this.ListeObjet = new ArrayList<IObjet>();
		this.PointAccesNord = pointAccesNord;
		this.PointAccesSud = pointAccesSud;
		this.PointAccesEst = pointAccesEst;
		this.PointAccesOuest = pointAccesOuest;
	}

}
