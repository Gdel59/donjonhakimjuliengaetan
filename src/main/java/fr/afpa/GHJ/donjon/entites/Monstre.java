package fr.afpa.GHJ.donjon.entites;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Monstre extends Personnage {
	boolean deplacer;

	public Monstre(int pv, int force, int nbrPieces) {
		super(pv, force, nbrPieces);
		deplacer = true;
	}

	public Monstre() {

	}

	@Override
	public String toString() {
		return "Dindon \nPV :" + this.getPv() + "\nForce :" + this.getForce() + "\nOr :" + this.getNbrPieces();
	}

}
