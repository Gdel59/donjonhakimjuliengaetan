package fr.afpa.GHJ.donjon.entites;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor

public class Joueur extends Personnage {

	private String nom;

	@Builder
	public Joueur(int pv, int force, int nbrPieces, String nom) {
		super(pv, force, nbrPieces);
		this.nom = nom;
		if("jugaha".equals(nom.toLowerCase())) {
			this.setForce(10000);
			this.setPv(10000);
		}
	}

	@Override
	public String toString() {
		return "Joueur \nPV :" + this.getPv() + "\nForce :" + this.getForce() + "\nOr :" + this.getNbrPieces();
	}

}
