package fr.afpa.GHJ.donjon.entites;

import java.util.Random;

public class BanditManchot implements IObjet {

	@Override
	public boolean effet(Joueur joueur) {

		int bm = new Random().nextInt(125) + 125;

		if (joueur.getNbrPieces() >= bm) {

			joueur.setNbrPieces(joueur.getNbrPieces() - bm);

			int aleatoire = new Random().nextInt(3);

			switch (aleatoire) {

			case 0:
				new PotionDeSoin().effet(joueur);
				new PotionDeSoin().effet(joueur);
				new PotionDeSoin().effet(joueur);

				break;

			case 1:
				new PotionDeForce().effet(joueur);
				new PotionDeForce().effet(joueur);
				new PotionDeForce().effet(joueur);

				break;

			case 2:
				new BourseOr().effet(joueur);
				new BourseOr().effet(joueur);
				new BourseOr().effet(joueur);
				new BourseOr().effet(joueur);
				new BourseOr().effet(joueur);

				break;

			}

		}
		return true;

	}

	@Override
	public String toString() {
		return "Bandit manchot";
	}
}
