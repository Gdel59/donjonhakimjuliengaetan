package fr.afpa.GHJ.donjon.entites;

public class SortieDonjon extends Salle {

	public SortieDonjon(Joueur joueur, boolean pointAccesNord, boolean pointAccesSud, boolean pointAccesEst,
			boolean pointAccesOuest) {
		super(joueur, pointAccesNord, pointAccesSud, pointAccesEst, pointAccesOuest);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "# # " + (this.isPointAccesNord() ? " " : "#") + " # #\n" + "# "
				+ (this.getJoueur() != null ? VERT + "P" : " ") + BLANC + "   " + ROUGE + "S" + BLANC
				+ " #\n" + (this.isPointAccesOuest() ? " " : "#") + "       " + (this.isPointAccesEst() ? " \n" : "#\n")
				+ "# "+MAGENTA+"BOSS"+BLANC+"  #\n" + "# # " + (this.isPointAccesSud() ? " " : "#") + " # #\n";
	}

}
