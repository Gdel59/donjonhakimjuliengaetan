package fr.afpa.GHJ.donjon.entites;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class BourseOr implements IObjet {

	@Override
	public boolean effet(Joueur joueur) {

		int aleatoire = new Random().nextInt(100);

		joueur.setNbrPieces(joueur.getNbrPieces() + aleatoire);

		System.out.println("Vous avez obtenu " + aleatoire + " pièce(s) d'or !");
		System.out.println("Votre total de pièces d'or s'élève maintenant à " + joueur.getNbrPieces() + " .");

		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;

	}

	@Override
	public String toString() {
		return "Bourse d'or";
	}
}
