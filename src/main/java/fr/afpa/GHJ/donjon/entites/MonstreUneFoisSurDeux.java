package fr.afpa.GHJ.donjon.entites;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class MonstreUneFoisSurDeux extends Monstre {
	boolean unSurDeux;

	public MonstreUneFoisSurDeux(int pv, int force, int nbrPieces) {
		super(pv, force, nbrPieces);
		this.unSurDeux = false;
	}

	public MonstreUneFoisSurDeux() {

	}
}
