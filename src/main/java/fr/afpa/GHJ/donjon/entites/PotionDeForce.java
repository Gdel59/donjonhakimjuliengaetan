package fr.afpa.GHJ.donjon.entites;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class PotionDeForce implements IObjet {

	@Override
	public boolean effet(Joueur joueur) {
		int aleatoire = new Random().nextInt(30);
		joueur.setForce(joueur.getForce() + aleatoire);
		System.out.println("Vous avez obtenu " + aleatoire + " de force !");
		System.out.println("votre niveau de force est actuellement a " + joueur.getForce());
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public String toString() {
		return "Potion de force";
	}
}
