package fr.afpa.GHJ.donjon.services;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import fr.afpa.GHJ.donjon.entites.Donjon;
import fr.afpa.GHJ.donjon.entites.Joueur;
import fr.afpa.GHJ.donjon.entites.Monstre;
import fr.afpa.GHJ.donjon.entites.Personnage;
import fr.afpa.GHJ.donjon.entites.Salle;
import fr.afpa.GHJ.donjon.ihm.AffichageMenuJoueur;

public class ServiceJoueur extends ServicePersonnage {

	/**
	 * Méthode qui permet de récupérer les pièces d'or du monstre vaincu (point de
	 * vie à 0) et de les ajouter au total de pièce d'or initial du joueur
	 * 
	 * @param joueur  joueur
	 * @param monstre monstre
	 * @return
	 */
	public static boolean recupererPiece(Joueur joueur, Monstre monstre) {
		if (monstre.getPv() < 1) {
			joueur.setNbrPieces(monstre.getNbrPieces() + joueur.getNbrPieces());
			try {
				System.out.println("Felicitation " + joueur.getNom() + "!!!!!tu as récupéré " + monstre.getNbrPieces()
						+ " pièces d'or sur le dindon.");
				TimeUnit.SECONDS.sleep(2);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Le total de ta bourse est maintenant de " + joueur.getNbrPieces() + " pièces d'or");
			monstre.setNbrPieces(0);
			return true;
		}
		return false;

	}

	/**
	 * Permet le deplacement du personnage
	 * 
	 * @param perso       le personnage
	 * @param donjon      le donjon
	 * @param salleDepart la salle d'ou vient le personnage
	 * @param in          un scanner
	 * @return true si le personnage s'est deplacer, false si non
	 */

	public static boolean deplacement(Personnage perso, Donjon donjon, Salle salleDepart, Scanner in) {
		boolean find = false;
		for (int i = 0; i < donjon.getSalle().length && !find; i++) {
			for (int j = 0; j < donjon.getSalle()[i].length && !find; j++) {
				if (salleDepart == donjon.getOneSalle(i, j)) {
					find = true;
				}
			}
		}
		AffichageMenuJoueur.affichageSeDeplacer(salleDepart);
		return ServiceSalle.DeplacementPersonnage(salleDepart, (Joueur) perso, donjon, in);
	}

	/**
	 * Méthode qui permet de regarder dans la direction d'un point d'accès afin de
	 * récupérer les informations de la salle ciblée (présence de monstres/d'objets)
	 * 
	 * @param joueur le joueur
	 * @param donjon le donjon
	 * @param salle  la salle actuelle
	 * @param in     un scanner
	 * @return true si le joueur a regarder, false si non
	 */
	public static boolean regarder(Joueur joueur, Donjon donjon, Salle salle, Scanner in) {
		char choix;
		System.out.println("Faites votre choix");
		AffichageMenuJoueur.affichageRegarder(salle);
		try {
			choix = in.nextLine().toUpperCase().charAt(0);
			switch (choix) {
			case 'N':
				Salle salleN = ServiceSalle.scanSalle(salle, donjon, joueur, 'N');
				System.out.println("Il y a " + salleN.getListeDeMonstre().size() + " dindon(s) dans la pièce.");
				for (int i = 0; i < salleN.getListeDeMonstre().size(); i++) {
					System.out.println(salleN.getListeDeMonstre().get(i).toString());
				}
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Il y a " + salleN.getListeObjet().size() + " objet(s) dans la pièce.");
				for (int i = 0; i < salleN.getListeObjet().size(); i++) {
					System.out.println(salleN.getListeObjet().get(i).toString());
				}
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return true;

			case 'S':
				Salle salleS = ServiceSalle.scanSalle(salle, donjon, joueur, 'S');
				System.out.println("Il y a " + salleS.getListeDeMonstre().size() + " dindon(s) dans la pièce.");
				for (int i = 0; i < salleS.getListeDeMonstre().size(); i++) {
					System.out.println(salleS.getListeDeMonstre().get(i).toString());
				}
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Il y a " + salleS.getListeObjet().size() + " objet(s) dans la pièce.");
				for (int i = 0; i < salleS.getListeObjet().size(); i++) {
					System.out.println(salleS.getListeObjet().get(i).toString());
				}
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return true;

			case 'E':
				Salle salleE = ServiceSalle.scanSalle(salle, donjon, joueur, 'E');
				System.out.println("Il y a " + salleE.getListeDeMonstre().size() + " dindon(s) dans la pièce.");
				for (int i = 0; i < salleE.getListeDeMonstre().size(); i++) {
					System.out.println(salleE.getListeDeMonstre().get(i).toString());
				}
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Il y a " + salleE.getListeObjet().size() + " objet(s) dans la pièce.");
				for (int i = 0; i < salleE.getListeObjet().size(); i++) {
					System.out.println(salleE.getListeObjet().get(i).toString());
				}
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return true;

			case 'O':
				Salle salleO = ServiceSalle.scanSalle(salle, donjon, joueur, 'O');
				System.out.println("Il y a " + salleO.getListeDeMonstre().size() + " dindon(s) dans la pièce.");
				for (int i = 0; i < salleO.getListeDeMonstre().size(); i++) {
					System.out.println(salleO.getListeDeMonstre().get(i).toString());
				}
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Il y a " + salleO.getListeObjet().size() + " objet(s) dans la pièce.");
				for (int i = 0; i < salleO.getListeObjet().size(); i++) {
					System.out.println(salleO.getListeObjet().get(i).toString());
				}
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return true;
			case 'P':
				System.out.println(joueur.toString());
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Il y a " + salle.getListeDeMonstre().size() + " dindon(s) dans la pièce.");
				for (int i = 0; i < salle.getListeDeMonstre().size(); i++) {
					System.out.println(salle.getListeDeMonstre().get(i).toString());
				}
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Il y a " + salle.getListeObjet().size() + " objet(s) dans la pièce.");
				for (int i = 0; i < salle.getListeObjet().size(); i++) {
					System.out.println(salle.getListeObjet().get(i).toString());
				}
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return true;
			default:
				return false;
			}
		} catch (StringIndexOutOfBoundsException e) {
			return false;
		}
	}

	/**
	 * Méthode qui permet d'infliger des dégâts au monstre ciblé en fonction des
	 * points de force du joueur
	 * 
	 * @param donjon  le donjon
	 * @param joueur  le joueur
	 * @param salle   la salle actuelle
	 * @param monstre le monstre cible
	 * @param in      un scanner
	 * @return true si le joueur a combattu, false si non
	 */

	public static boolean combattre(Donjon donjon, Joueur joueur, Salle salle, Monstre monstre) {
		monstre.setPv(monstre.getPv() - joueur.getForce());
		System.out.println("Vous avez enlevé " + joueur.getForce() + " points de vie au dindon. Il lui reste "
				+ monstre.getPv() + " point(s) de vie");
		statutJoueur(joueur);
		ServiceMonstre.statutMonstre(monstre);
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (!ServiceSalle.monstreMort(salle, joueur)) {
			new ServiceMonstre().riposte(donjon, monstre, joueur, salle);
			return true;
		}
		return false;
	}

	/**
	 * Méthode qui permet de déterminer si un objet est présent dans la salle et si
	 * tel est le cas, la méthode permet de choisir et utiliser l'objet voulu
	 * 
	 * @param donjon le donjon
	 * @param joueur le joueur
	 * @param salle  la salle actuelle
	 * @param in     un scanner
	 * @return true si l'objet a ete utiliser, false si non
	 */
	public static boolean utiliserObjets(Donjon donjon, Joueur joueur, Salle salle, int choix) {
		salle.getListeObjet().get(choix - 3).effet(joueur);
		salle.getListeObjet().remove(choix - 3);
		return true;
	}

	/**
	 * Affiche le statut du joueur
	 * 
	 * @param joueur le joueur a afficher
	 */
	public static void statutJoueur(Joueur joueur) {
		System.out.println("PV Joueur : " + joueur.getPv() + " point(s) de vie");
		System.out.println("PF Joueur : " + joueur.getForce() + " points de force");
		System.out.println("Pieces d'or : " + joueur.getNbrPieces() + " pieces");
	}

}
