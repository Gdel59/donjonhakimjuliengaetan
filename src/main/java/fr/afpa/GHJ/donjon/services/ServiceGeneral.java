package fr.afpa.GHJ.donjon.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import fr.afpa.GHJ.donjon.entites.Joueur;

public class ServiceGeneral {

	/**
	 * permet de sauvegarder le score du joueur
	 * 
	 * @param joueur le joueur a sauvegarder
	 */
	public static void sauvegarderJoueur(Joueur joueur) {
		try {
			FileWriter fw = new FileWriter(System.getenv("JUGAHA") + "\\RECORD", true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(joueur.getNom() + ";" + joueur.getNbrPieces() + ";" + LocalDate.now());
			bw.newLine();
			bw.close();
			fw.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Permet de charger les donnees enregistrer
	 * 
	 * @return la liste des donnee charger
	 */
	public static List<String> chargerDonnees() {
		try {
			List<String> record = new ArrayList<String>();
			FileReader fr = new FileReader(System.getenv("JUGAHA") + "\\RECORD");
			BufferedReader br = new BufferedReader(fr);
			while (br.ready()) {
				String a = br.readLine();
				if (a.split(";").length == 3)
					record.add(a);
			}
			Collections.sort(record, new Comparator<String>() {
				@Override
				public int compare(String s1, String s2) {
					int i1 = Integer.parseInt(s1.split(";")[1]);
					int i2 = Integer.parseInt(s2.split(";")[1]);
					int cmp = Integer.compare(i1, i2);
					if (cmp != 0) {
						return cmp;
					}
					return s1.compareTo(s2);
				}
			});
			br.close();
			fr.close();
			return record;
		} catch (IOException e) {
			System.out.println("La variable d'environement est incorrecte");
		}

		return null;

	}
}
