package fr.afpa.GHJ.donjon.services;

import fr.afpa.GHJ.donjon.entites.Joueur;
import fr.afpa.GHJ.donjon.entites.Monstre;
import fr.afpa.GHJ.donjon.entites.Salle;
import java.util.Scanner;
import fr.afpa.GHJ.donjon.entites.Donjon;

public class ServiceSalle {

	/**
	 * permet de recuperer les coordonee de la salle choisit
	 * 
	 * @param donjon         le donjon de la recherche
	 * @param salleRecherche la salle rechercher
	 * @return la salle rechercher, null si non
	 */
	public static int[] coordonneeSalle(Donjon donjon, Salle salleRecherche) {
		int[] coordonnee = new int[2];
		for (int i = 0; i < donjon.getSalle().length; i++) {
			for (int j = 0; j < donjon.getSalle()[0].length; j++) {
				if (donjon.getOneSalle(i, j) == salleRecherche) {
					coordonnee[0] = i;
					coordonnee[1] = j;
				}
			}
		}
		return coordonnee;
	}

	/**
	 * Permet de deplacer un joueur
	 * 
	 * @param salle  la salle de depart du joueur
	 * @param joueur le joueur a deplacer
	 * @param donjon le donjon
	 * @param in     un scanner
	 * @return true si le deplacement a ete effectuer, false si non
	 */
	public static boolean DeplacementPersonnage(Salle salle, Joueur joueur, Donjon donjon, Scanner in) {

		int x = coordonneeSalle(donjon, salle)[0];
		int y = coordonneeSalle(donjon, salle)[1];
		char choixDeplacement;
		System.out.println("Veuillez saisir votre choix de déplacement");
		try {
			choixDeplacement = in.nextLine().toUpperCase().charAt(0);

			switch (choixDeplacement) {

			case 'N':
				if (salle.isPointAccesNord()) {
					donjon.getOneSalle(x - 1, y).setJoueur(salle.getJoueur());
					salle.setJoueur(null);
					return true;
				} else {
					System.out.println("cette direction est indisponible");
					return false;
				}

			case 'S':
				if (salle.isPointAccesSud()) {
					donjon.getOneSalle(x + 1, y).setJoueur(salle.getJoueur());
					salle.setJoueur(null);
					return true;
				} else {
					System.out.println("cette direction est indisponible");
					return false;
				}

			case 'E':
				if (salle.isPointAccesEst()) {
					donjon.getOneSalle(x, y + 1).setJoueur(salle.getJoueur());
					salle.setJoueur(null);
					return true;
				} else {
					System.out.println("cette direction est indisponible");
					return false;
				}

			case 'O':
				if (salle.isPointAccesOuest()) {
					donjon.getOneSalle(x, y - 1).setJoueur(salle.getJoueur());
					salle.setJoueur(null);
					return true;
				} else {
					System.out.println("cette direction est indisponible");
					return false;
				}

			default:
				return false;

			}
		} catch (StringIndexOutOfBoundsException e) {
			return false;
		}
	}

	/**
	 * Permet d'enregistrer la mort d'un monstre
	 * 
	 * @param salle  la salle
	 * @param joueur le joueur
	 * @return true s'il est mort, false si non
	 */
	public static boolean monstreMort(Salle salle, Joueur joueur) {
		for (Monstre monstre : salle.getListeDeMonstre()) {
			if (monstre.getPv() < 1) {
				ServiceJoueur.recupererPiece(joueur, monstre);
				salle.getListeDeMonstre().remove(monstre);
				return true;
			}

		}
		return false;
	}

	/**
	 * permet de scanner la salle
	 * 
	 * @param salle  la salle a scanner
	 * @param donjon le donjon
	 * @param joueur le joueur
	 * @param choix  le choix de la salle a scanner
	 * @return une salle pour le scan
	 */
	public static Salle scanSalle(Salle salle, Donjon donjon, Joueur joueur, char choix) {
		int x = coordonneeSalle(donjon, salle)[0];
		int y = coordonneeSalle(donjon, salle)[1];

		if (salle != null) {

			switch (choix) {

			case 'N':

				return donjon.getOneSalle(x - 1, y);

			case 'S':

				return donjon.getOneSalle(x + 1, y);

			case 'E':

				return donjon.getOneSalle(x, y + 1);

			case 'O':

				return donjon.getOneSalle(x, y - 1);

			}

		}
		return null;

	}

}
