package fr.afpa.GHJ.donjon.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import fr.afpa.GHJ.donjon.controles.ControleDonjon;
import fr.afpa.GHJ.donjon.entites.BanditManchot;
import fr.afpa.GHJ.donjon.entites.BourseOr;
import fr.afpa.GHJ.donjon.entites.Donjon;
import fr.afpa.GHJ.donjon.entites.EDirection;
import fr.afpa.GHJ.donjon.entites.Salle;
import fr.afpa.GHJ.donjon.entites.EntreeDonjon;
import fr.afpa.GHJ.donjon.entites.IObjet;
import fr.afpa.GHJ.donjon.entites.Joueur;
import fr.afpa.GHJ.donjon.entites.Monstre;
import fr.afpa.GHJ.donjon.entites.MonstreAlea;
import fr.afpa.GHJ.donjon.entites.MonstreUneFoisSurDeux;
import fr.afpa.GHJ.donjon.entites.PotionDeForce;
import fr.afpa.GHJ.donjon.entites.PotionDeSoin;
import fr.afpa.GHJ.donjon.entites.SortieDonjon;
import fr.afpa.GHJ.donjon.ihm.AffichageDonjon;
import fr.afpa.GHJ.donjon.ihm.AffichageMenuJoueur;

public class ServiceDonjon {
	private final int x;
	private final int y;
	private final int[][] maze;

	/**
	 * constructeur du labyrinthe
	 * 
	 * @param x taille x
	 * @param y taille y
	 */
	public ServiceDonjon(int x, int y) {
		this.x = x;
		this.y = y;
		maze = new int[this.x][this.y];
		generateMaze(0, 0);
	}

	/**
	 * recherche la salle finale du donjon
	 * 
	 * @param donjon le donjon
	 * @return la salle finale
	 */
	public static Salle rechercheSalleFinale(Donjon donjon) {
		int[] c = new int[2];
		for (int i = 0; i < donjon.getSalle().length; i++) {
			for (int j = 0; j < donjon.getSalle()[0].length; j++) {
				if (donjon.getOneSalle(i, j) instanceof SortieDonjon) {
					c[0] = i;
					c[1] = j;
				}
			}
		}
		return donjon.getOneSalle(c[0], c[1]);
	}

	/**
	 * recherche une salle via le joueur
	 * 
	 * @param donjon le donjon
	 * @param joueur le joueur rechercher
	 * @return la salle ou le joueur se trouve
	 */
	public static Salle rechercheSalleViaJoueur(Donjon donjon, Joueur joueur) {
		for (Salle[] salles : donjon.getSalle()) {
			for (Salle salle : salles) {
				if (salle.getJoueur() == joueur)
					return salle;
			}
		}
		return null;
	}

	/**
	 * recherche une salle via un monstre
	 * 
	 * @param donjon  le donjon
	 * @param monstre le monstre rechercher
	 * @return la salle ou le monstre se trouve
	 */
	public static Salle rechercheSalleViaMonstre(Donjon donjon, Monstre monstre) {
		for (Salle[] salles : donjon.getSalle()) {
			for (Salle salle : salles) {
				for (Monstre monstres : salle.getListeDeMonstre()) {
					if (monstres == monstre) {
						return salle;
					}
				}
			}
		}
		return null;
	}

	/**
	 * permet d'efectuer un tour de jeu
	 * 
	 * @param donjon le donjon
	 * @param joueur le joueur
	 * @param in     un scanner
	 */
	public static void tourDeJeu(Donjon donjon, Joueur joueur, Scanner in) {
		AffichageDonjon.titre();
		AffichageDonjon.affichageEtatDonjon(donjon);
		Salle salle = ServiceDonjon.rechercheSalleViaJoueur(donjon, joueur);
		AffichageMenuJoueur.affichageActions(donjon, salle, in, joueur);
		System.out.println("Choix :");
		try {
			int choix = in.nextInt();
			in.nextLine();

			switch (choix) {
			case 1: {
				ServiceJoueur.regarder(joueur, donjon, salle, in);
				break;
			}
			case 2: {
				// Si il y a au moins un monstre dans la salle
				if (salle.getListeDeMonstre().size() > 0) {
					// TODO choix du monstre
					Monstre monstre = salle.getListeDeMonstre().get(0);
					ServiceJoueur.combattre(donjon, joueur, salle, monstre);
				} else {
					ServiceJoueur.deplacement(joueur, donjon, salle, in);
				}
				break;
			}
			case 3: {
				if (salle.getListeDeMonstre().size() < 1 && salle.getListeObjet().size() > 0) {
					salle.getListeObjet().get(0).effet(joueur);
					salle.getListeObjet().remove(0);
				}
				break;
			}
			case 4: {
				if (salle.getListeDeMonstre().size() < 1 && salle.getListeObjet().size() > 1) {
					salle.getListeObjet().get(1).effet(joueur);
					salle.getListeObjet().remove(1);
				}
				break;
			}
			case 5: {
				if (salle.getListeDeMonstre().size() < 1 && salle.getListeObjet().size() > 2) {
					salle.getListeObjet().get(2).effet(joueur);
					salle.getListeObjet().remove(2);
				}
				break;
			}
			case 6: {
				if (salle.getListeDeMonstre().size() < 1 && salle.getListeObjet().size() > 3) {
					salle.getListeObjet().get(3).effet(joueur);
					salle.getListeObjet().remove(3);
				}
				break;
			}
			case 7: {
				if (salle.getListeDeMonstre().size() < 1 && salle.getListeObjet().size() == 5) {
					salle.getListeObjet().get(4).effet(joueur);
					salle.getListeObjet().remove(4);
				}
				break;
			}

			default:
				break;
			}
		} catch (InputMismatchException e) {
			try {
				TimeUnit.SECONDS.sleep(2);
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			System.out.println("erreur de saisie");
			in.nextLine();
		}
		monsterTurn(donjon, joueur);
	}

	/**
	 * permet de faire jouer un monstre
	 * 
	 * @param donjon le donjon
	 * @param joueur le joueur
	 */
	public static void monsterTurn(Donjon donjon, Joueur joueur) {
		activeDeplacementMonstre(donjon);
		// Tour des monstres
		try {
			for (Salle[] salles : donjon.getSalle()) {
				for (Salle salleDepart : salles) {
					int i = 0;
					while(i < salleDepart.getListeDeMonstre().size()) {
						Monstre m = salleDepart.getListeDeMonstre().get(i);
						if (!m.isDeplacer())
							i++;
						if (m instanceof MonstreAlea) {
							ServiceMonstre.deplacementMonstre(m, donjon, salleDepart, joueur);
						}
						else
							i++;
//						if (m instanceof MonstreUneFoisSurDeux) {
//							ServiceMonstre.deplacementUneFoisSurDeux((MonstreUneFoisSurDeux) m, donjon, salleDepart, joueur);
//						}
//						else
//							i++;
					}
				}
			}
		} catch (ConcurrentModificationException e) {

		}

	}
	
	/**
	 * active le deplacement de tout les monstres
	 * 
	 * @param donjon la donjon
	 */
	private static void activeDeplacementMonstre(Donjon donjon) {
		for (Salle[] salles : donjon.getSalle()) {
			for (Salle salle : salles) {
				for (Monstre monstre : salle.getListeDeMonstre()) {
					monstre.setDeplacer(true);
				}
			}
		}
	}

	/**
	 * permet de créer le donjon
	 * 
	 * @return un nouveau donjon
	 */
	public Donjon creationDonjon() {
		Donjon donjon = new Donjon(y, x);
		ServiceDonjon maze = new ServiceDonjon(x, y);
		char[][] a = maze.display2();
		String[] lines = maze.display().split("\n");
		char[][] lab = decimateHorizontally(lines);
		solveMaze(lab, a);
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				donjon.setOneSalle(convertirSalle(a[i][j]), i, j);
				if (i != 0 || j != 0) {
					donjon.getOneSalle(i, j).setListeDeMonstre(ajoutMonstreOuPas());
					if (!(donjon.getOneSalle(i, j) instanceof SortieDonjon))
						donjon.getOneSalle(i, j).setListeObjet(ajoutObjetOuPas());
					if (donjon.getOneSalle(i, j) instanceof SortieDonjon) {
						List<Monstre> grosPoulet = new ArrayList<Monstre>();
						grosPoulet.add(new Monstre(a.length * a[0].length * 20, a.length * a[0].length * 2,
								a.length * a[0].length * 100));
						donjon.getOneSalle(i, j).setListeDeMonstre(grosPoulet);
					}
				}
			}
		}
		return donjon;
	}

	/**
	 * permet de convertir un char en salle
	 * 
	 * @param c le char a convertir
	 * @return une nouvelle salle
	 */
	public Salle convertirSalle(char c) {
		switch (c) {
		case 'a':
			return new Salle(null, true, false, false, false);
		case 'b':
			return new Salle(null, false, false, true, false);
		case 'c':
			return new Salle(null, false, true, false, false);
		case 'd':
			return new Salle(null, false, false, false, true);
		case 'e':
			return new Salle(null, true, false, true, false);
		case 'f':
			return new Salle(null, false, true, true, false);
		case 'g':
			return new Salle(null, false, true, false, true);
		case 'h':
			return new Salle(null, true, false, false, true);
		case 'i':
			return new Salle(null, true, false, true, true);
		case 'j':
			return new Salle(null, true, true, true, false);
		case 'k':
			return new Salle(null, false, true, true, true);
		case 'l':
			return new Salle(null, true, true, false, true);
		case 'm':
			return new Salle(null, true, true, false, false);
		case 'n':
			return new Salle(null, false, false, true, true);
		case 'o':
			return new Salle(null, true, true, true, true);
		case 'p':
			return new EntreeDonjon(null, false, true, false, false);
		case 'q':
			return new EntreeDonjon(null, false, false, true, false);
		case 'r':
			return new EntreeDonjon(null, false, true, true, false);
		case 's':
			return new SortieDonjon(null, true, false, false, false);
		case 't':
			return new SortieDonjon(null, false, true, false, false);
		case 'u':
			return new SortieDonjon(null, false, false, true, false);
		case 'v':
			return new SortieDonjon(null, false, false, false, true);
		default:
			return null;
		}
	}

	/**
	 * creer entre 0 et 3 monstres
	 * 
	 * @return une liste de monstre
	 */
	public static List<Monstre> ajoutMonstreOuPas() {
		List<Monstre> monstre = new ArrayList<Monstre>();
		int nbrMonstre = new Random().nextInt(14);
		for (int i = 11; i <= nbrMonstre; i++) {
			int aleatoire = 0;//new Random().nextInt(2);

			switch (aleatoire) {
			case 0:
				monstre.add(new MonstreAlea(new Random().nextInt(75) + 75, new Random().nextInt(20) + 5,
						new Random().nextInt(100)));
				break;

			case 1:
				monstre.add(new MonstreUneFoisSurDeux(new Random().nextInt(75) + 75, new Random().nextInt(20) + 5,
						new Random().nextInt(100)));

				break;
			}
		}

		return monstre;
	}

	/**
	 * creer entre 0 et 5 objets
	 * 
	 * @return une liste d'objet
	 */
	public List<IObjet> ajoutObjetOuPas() {
		List<IObjet> objet = new ArrayList<IObjet>();
		int nbrObjet = new Random().nextInt(16);
		for (int i = 11; i <= nbrObjet; i++) {
			switch (new Random().nextInt(4)) {
			case 0:
				objet.add(new PotionDeSoin());
				break;
			case 1:
				objet.add(new PotionDeForce());
				break;
			case 2:
				objet.add(new BourseOr());
				break;
			case 3:
				objet.add(new BanditManchot());
				break;
			default:
				break;
			}
		}

		return objet;
	}

	/**
	 * affiche la carte vierge du labyrinthe
	 * 
	 * @return la carte
	 */
	public String display() {
		String res = "";
		for (int i = 0; i < y; i++) {
			// draw the north edge
			for (int j = 0; j < x; j++) {
				res += (maze[j][i] & 1) == 0 ? "+---" : "+   ";
			}
			res += "+\n";
			// draw the west edge
			for (int j = 0; j < x; j++) {
				res += (maze[j][i] & 8) == 0 ? "|   " : "    ";
			}
			res += "|\n";
		}
		// draw the bottom line
		for (int j = 0; j < x; j++) {
			res += "+---";
		}
		res += "+\n";
		return res;
	}

	/**
	 * convertis la carte pour fabriquer le donjon
	 * 
	 * @return
	 */
	public char[][] display2() {
		char[][] a = new char[this.y][this.x];

		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				if (i == 0 && j == 0)
					a[j][i] = change(maze[i][j], true, false);
				else
					a[j][i] = change(maze[i][j], false, false);
			}
		}
		return a;
	}

	/**
	 * change un nombre en code pour la fabrication
	 * 
	 * @param valeur la valeur du nombre
	 * @param start  true si c'est pour une salle de depart, false si non
	 * @param end    true si c'est pour une salle finale, false si non
	 * @return la valeur coder
	 */
	public static char change(int valeur, boolean start, boolean end) {
		if (start)
			switch (valeur) {
			case 6:
				return 'r';
			case 4:
				return 'q';
			case 2:
				return 'p';
			}
		if (end)
			switch (valeur) {
			case 97:
				return 's';
			case 98:
				return 'u';
			case 99:
				return 't';
			case 100:
				return 'v';
			}
		switch (valeur) {
		case 1:
			return 'a';
		case 2:
			return 'c';
		case 3:
			return 'm';
		case 4:
			return 'b';
		case 5:
			return 'e';
		case 6:
			return 'f';
		case 7:
			return 'j';
		case 8:
			return 'd';
		case 9:
			return 'h';
		case 10:
			return 'g';
		case 11:
			return 'l';
		case 12:
			return 'n';
		case 13:
			return 'i';
		case 14:
			return 'k';
		case 15:
			return 'o';
		default:
			return '0';
		}
	}

	/**
	 * genere le labyrinthe
	 * 
	 * @param cx
	 * @param cy
	 */
	private void generateMaze(int cx, int cy) {
		EDirection[] dirs = EDirection.values();
		Collections.shuffle(Arrays.asList(dirs));
		for (EDirection dir : dirs) {
			int nx = cx + dir.dx;
			int ny = cy + dir.dy;
			if (ControleDonjon.between(nx, x) && ControleDonjon.between(ny, y) && (maze[nx][ny] == 0)) {
				maze[cx][cy] |= dir.bit;
				maze[nx][ny] |= dir.opposite.bit;
				generateMaze(nx, ny);
			}
		}
	}

	// Resolution

	/**
	 * reduit le labyrinthe
	 * 
	 * @param lines les lignes du labyrinthe
	 * @return un tableau a deux dimension du labyrinthe
	 */
	public static char[][] decimateHorizontally(String[] lines) {
		final int width = (lines[0].length() + 1) / 2;
		char[][] c = new char[lines.length][width];
		for (int i = 0; i < lines.length; i++)
			for (int j = 0; j < width; j++)
				c[i][j] = lines[i].charAt(j * 2);
		return c;
	}

	/**
	 * methode inverse de decimateHorizontally
	 * 
	 * @param maze le labyrinthe
	 * @return
	 */
	public static String[] expandHorizontally(char[][] maze) {
		char[] tmp = new char[3];
		String[] lines = new String[maze.length];
		for (int i = 0; i < maze.length; i++) {
			StringBuilder sb = new StringBuilder(maze[i].length * 2);
			for (int j = 0; j < maze[i].length; j++)
				if (j % 2 == 0)
					sb.append(maze[i][j]);
				else {
					tmp[0] = tmp[1] = tmp[2] = maze[i][j];
					if (tmp[1] == '#')
						tmp[0] = tmp[2] = ' ';
					sb.append(tmp);
				}
			lines[i] = sb.toString();
		}
		return lines;
	}

	/**
	 * resoud le labyrinthe de facon recursive
	 * 
	 * @param maze le labyrinthe
	 * @param x    la coordonnee x
	 * @param y    la coordonnee y
	 * @param d
	 * @return true si le labyrinthe est resolu, false si non
	 */
	public static boolean solveMazeRecursively(char[][] maze, int x, int y, int d) {
		boolean ok = false;
		for (int i = 0; i < 4 && !ok; i++)
			try {
				if (i != d)
					switch (i) {
// 0 = up, 1 = right, 2 = down, 3 = left
					case 0:
						if (maze[y - 1][x] == ' ')
							ok = solveMazeRecursively(maze, x, y - 2, 2);
						break;
					case 1:
						if (maze[y][x + 1] == ' ')
							ok = solveMazeRecursively(maze, x + 2, y, 3);
						break;
					case 2:
						if (maze[y + 1][x] == ' ')
							ok = solveMazeRecursively(maze, x, y + 2, 0);
						break;
					case 3:
						if (maze[y][x - 1] == ' ')
							ok = solveMazeRecursively(maze, x - 2, y, 1);
						break;
					}
			} catch (ArrayIndexOutOfBoundsException e) {
				return false;
			}
// check for end condition
		if (x == 1 && y == 1)
			ok = true;
// once we have found a solution, draw it as we unwind the recursion
		if (ok) {
			maze[y][x] = '#';
			switch (d) {
			case 0:
				maze[y - 1][x] = '#';
				break;
			case 1:
				maze[y][x + 1] = '#';
				break;
			case 2:
				maze[y + 1][x] = '#';
				break;
			case 3:
				maze[y][x - 1] = '#';
				break;
			}
		}
		return ok;
	}

	/**
	 * resoud le labyrinthe
	 * 
	 * @param maze
	 * @param a
	 */
	private static void solveMaze(char[][] maze, char[][] a) {
		int[][] tab = new int[a.length * a[0].length][2];
		int indice = 0;
		// calcul du nombre de sortie
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				if (i == 0 || j == 0 || i == a.length - 1 || j == a[0].length - 1) {
					if (a[i][j] == 'a' || a[i][j] == 'b' || a[i][j] == 'c' || a[i][j] == 'd') {
						tab[indice][1] = i * 2 + 1;
						tab[indice++][0] = j * 2 + 1;
					}
				}
			}
		}

		// affichage de tout les position des sortie
		for (int i = 0; i < tab.length; i++) {
			if (tab[i][0] != 0 && tab[i][1] != 0) {
			}
		}

		for (int k = 0; k < tab.length; k++) {
			indice = 0;
			for (int i = 1; i < maze.length - 1; i += 2) {
				for (int j = 1; j < maze[0].length - 1; j += 2) {
					if (i == 1 || j == 1 || i == maze.length - 2 || j == maze[0].length - 2) {
						solveMazeRecursively(maze, tab[indice][0], tab[indice][1], -1);
						indice++;
					}
				}
			}
		}
		indice = 0;
		// modification de l'arrivee
		a[tab[indice][1] / 2][tab[indice][0] / 2] = change(a[tab[indice][1] / 2][tab[indice][0] / 2], false, true);

	}

}
