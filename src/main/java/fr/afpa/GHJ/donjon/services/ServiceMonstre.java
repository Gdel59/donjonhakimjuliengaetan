package fr.afpa.GHJ.donjon.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import fr.afpa.GHJ.donjon.entites.Donjon;
import fr.afpa.GHJ.donjon.entites.Joueur;
import fr.afpa.GHJ.donjon.entites.Monstre;
import fr.afpa.GHJ.donjon.entites.MonstreUneFoisSurDeux;
import fr.afpa.GHJ.donjon.entites.Salle;

public class ServiceMonstre extends ServicePersonnage {

	/**
	 * Permet de faire riposter un monstre s'il est encore vivant
	 * 
	 * @param donjon  le donjon
	 * @param monstre le monstre qui riposte
	 * @param joueur  le joueur qui va recevoir la riposte
	 * @param salle   la salle dans laquelle les deux sont
	 * @return true si la riposte a ete effectuer, false si non
	 */
	public boolean riposte(Donjon donjon, Monstre monstre, Joueur joueur, Salle salle) {

		if (monstre.getPv() > 0) {
			joueur.setPv(joueur.getPv() - monstre.getForce());
			System.out.println("Le monstre vous a retiré " + monstre.getForce() + " points de vie");
			try {
				TimeUnit.SECONDS.sleep(3);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}
		}
		return false;

	}

	/**
	 * obtient l'indice d'un monstre dans la liste
	 * 
	 * @param m       la liste de monstre
	 * @param monstre le monstre rechercher
	 * @return l'indice du monstre
	 */
	public static int getIndiceMonstre(List<Monstre> m, Monstre monstre) {
		for (int i = 0; i < m.size(); i++) {
			if (m.get(i) == monstre)
				return i;
		}
		return -1;
	}

	/**
	 * Permet d'afficher le statut d'un monstre
	 * 
	 * @param monstre le monstre a afficher
	 */
	public static void statutMonstre(Monstre monstre) {
		System.out.println("PV Dindon : " + monstre.getPv() + " point(s) de vie");
		System.out.println("PF Dindon : " + monstre.getForce() + " points de force");
		System.out.println("Pieces d'or : " + monstre.getNbrPieces() + " pieces");

	}

	public static boolean deplacementUneFoisSurDeux(MonstreUneFoisSurDeux monstre, Donjon donjon, Salle salleDepart,
			Joueur joueur) {
		if (ServiceDonjon.rechercheSalleViaMonstre(donjon, monstre).equals(ServiceDonjon.rechercheSalleViaJoueur(donjon, joueur)))
			monstre.setDeplacer(false);
		if (!monstre.isDeplacer()) {
			return false;
		}
//		else if (monstre.isUnSurDeux()) {
//			deplacementMonstre(monstre, donjon, salleDepart, joueur);
//			monstre.setUnSurDeux(false);
//			return true;
//		}
//		else {
			Salle salleJoueur = ServiceDonjon.rechercheSalleViaJoueur(donjon, joueur);
			Salle salleMonstre = ServiceDonjon.rechercheSalleViaMonstre(donjon, monstre);
			unPasVersLeJoueur(donjon, salleJoueur, salleMonstre);
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			monstre.setUnSurDeux(true);
			return true;
//		}
	}

	public static boolean unPasVersLeJoueur(Donjon donjon, Salle salleJoueur, Salle salleMonstre) {
		List<String> chemin = new ArrayList<String>();
		char direction = chercherJoueurDansDonjon(donjon, salleJoueur, salleMonstre, chemin);
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		switch (direction) {
		case 'n':
			System.out.println(chemin);
			System.out.println("vers le nord");
			break;
		case 's':
			System.out.println(chemin);
			System.out.println("vers le sud");
			break;
		case 'e':
			System.out.println(chemin);
			System.out.println("vers l'est");
			break;
		case 'o':
			System.out.println(chemin);
			System.out.println("vers le sud");
			break;
		default:
//			chemin.remove(chemin.size()-1);
			System.out.println(chemin);
			System.out.println("vers nulle part");
			break;
		}
		return false;
	}

	public static char chercherJoueurDansDonjon(Donjon donjon, Salle salleJoueur, Salle salleActuelle, List<String> chemin) {
		System.out.println(chemin);
		if (salleActuelle == salleJoueur && chemin.size() > 0) {
			return chemin.get(0).charAt(0);
		} else if(chemin.size() == 0) {
			return '0';
		}
//		if(chemin.size() > 0)
			for (int i = 0; i < 4; i++) {
				if (i == 0 && salleActuelle.isPointAccesNord() && chemin.get(chemin.size()-1) != "s") {
					chemin.add("n");
					return chercherJoueurDansDonjon(donjon, salleJoueur, donjon.getOneSalle(ServiceSalle.coordonneeSalle(donjon, salleActuelle)[0]-1, ServiceSalle.coordonneeSalle(donjon, salleActuelle)[1]), chemin);
				} else if (i == 1 && salleActuelle.isPointAccesSud() && chemin.get(chemin.size()-1) != "n") {
					chemin.add("s");
					return chercherJoueurDansDonjon(donjon, salleJoueur, donjon.getOneSalle(ServiceSalle.coordonneeSalle(donjon, salleActuelle)[0]+1, ServiceSalle.coordonneeSalle(donjon, salleActuelle)[1]), chemin);
				} else if (i == 2 && salleActuelle.isPointAccesEst() && chemin.get(chemin.size()-1) != "o") {
					chemin.add("e");
					return chercherJoueurDansDonjon(donjon, salleJoueur, donjon.getOneSalle(ServiceSalle.coordonneeSalle(donjon, salleActuelle)[0], ServiceSalle.coordonneeSalle(donjon, salleActuelle)[1]+1), chemin);
				} else if (i == 3 && salleActuelle.isPointAccesOuest() && chemin.get(chemin.size()-1) != "e") {
					chemin.add("o");
					return chercherJoueurDansDonjon(donjon, salleJoueur, donjon.getOneSalle(ServiceSalle.coordonneeSalle(donjon, salleActuelle)[0], ServiceSalle.coordonneeSalle(donjon, salleActuelle)[1]-1), chemin);
				}
			}
		chemin.add("0");
		return '0';
	}

	public static boolean deplacementMonstre(Monstre monstre, Donjon donjon, Salle salleDepart, Joueur joueur) {
		if (ServiceDonjon.rechercheSalleViaMonstre(donjon, monstre).equals(ServiceDonjon.rechercheSalleViaJoueur(donjon, joueur)))
			monstre.setDeplacer(false);
		if (!monstre.isDeplacer()) {
			return false;
		}
		int x = ServiceSalle.coordonneeSalle(donjon, salleDepart)[0];
		int y = ServiceSalle.coordonneeSalle(donjon, salleDepart)[1];
		if (!(ServiceDonjon.rechercheSalleViaMonstre(donjon, monstre).equals(ServiceDonjon.rechercheSalleViaJoueur(donjon, joueur)))) {
		while (true) {
			int choixDeplacement = new Random().nextInt(4);
				switch (choixDeplacement) {
				case 0:
					if (salleDepart.isPointAccesNord()) {
						donjon.getOneSalle(x - 1, y).getListeDeMonstre().add(monstre);
						salleDepart.getListeDeMonstre().remove(getIndiceMonstre(salleDepart.getListeDeMonstre(), monstre));
						monstre.setDeplacer(false);
						return true;
					}
					break;
				case 1:
					if (salleDepart.isPointAccesSud()) {
						donjon.getOneSalle(x + 1, y).getListeDeMonstre().add(monstre);
						salleDepart.getListeDeMonstre().remove(getIndiceMonstre(salleDepart.getListeDeMonstre(), monstre));
						monstre.setDeplacer(false);
						return true;
					}
					break;
				case 2:
					if (salleDepart.isPointAccesEst()) {
						donjon.getOneSalle(x, y + 1).getListeDeMonstre().add(monstre);
						salleDepart.getListeDeMonstre().remove(getIndiceMonstre(salleDepart.getListeDeMonstre(), monstre));
						monstre.setDeplacer(false);
						return true;
					}
					break;
				case 3:
					if (salleDepart.isPointAccesOuest()) {
						donjon.getOneSalle(x, y - 1).getListeDeMonstre().add(monstre);
						salleDepart.getListeDeMonstre().remove(getIndiceMonstre(salleDepart.getListeDeMonstre(), monstre));
						monstre.setDeplacer(false);
						return true;
					}
					break;
				}
			}
		}
		else
			return false;
	}
}
