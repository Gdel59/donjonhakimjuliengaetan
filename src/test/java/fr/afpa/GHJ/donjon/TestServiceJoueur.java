package fr.afpa.GHJ.donjon;

import org.junit.Test;

import fr.afpa.GHJ.donjon.entites.Donjon;
import fr.afpa.GHJ.donjon.entites.Joueur;
import fr.afpa.GHJ.donjon.entites.Monstre;
import fr.afpa.GHJ.donjon.entites.Salle;
import fr.afpa.GHJ.donjon.services.ServiceJoueur;
import junit.framework.TestCase;

public class TestServiceJoueur extends TestCase {

	@Test
	public void testCombattre() {
		Joueur joueur1 = new Joueur(10, 10, 10, "abc");
		Monstre monstre1 = new Monstre(20, 2, 100);
		Donjon donjon1 = new Donjon(1, 1);
		Salle salle1 = new Salle(joueur1, true, false, false, true);
		salle1.getListeDeMonstre().add(monstre1);
		donjon1.setOneSalle(salle1, 0, 0);

		assertTrue(ServiceJoueur.combattre(donjon1, joueur1, salle1, monstre1));
		assertFalse(ServiceJoueur.combattre(donjon1, joueur1, salle1, monstre1));

	}

	@Test
	public void testRecupererPieces() {
		Joueur joueur1 = new Joueur(10, 10, 10, "abc");
		Monstre monstre1 = new Monstre(20, 2, 100);
		Donjon donjon1 = new Donjon(1, 1);
		Salle salle1 = new Salle(joueur1, true, false, false, true);
		salle1.getListeDeMonstre().add(monstre1);
		donjon1.setOneSalle(salle1, 0, 0);

		assertFalse(ServiceJoueur.recupererPiece(joueur1, monstre1));

	}
}
