package fr.afpa.GHJ.donjon;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import fr.afpa.GHJ.donjon.entites.Donjon;
import fr.afpa.GHJ.donjon.entites.EntreeDonjon;
import fr.afpa.GHJ.donjon.entites.Joueur;
import fr.afpa.GHJ.donjon.entites.Monstre;
import fr.afpa.GHJ.donjon.entites.MonstreAlea;
import fr.afpa.GHJ.donjon.entites.MonstreUneFoisSurDeux;
import fr.afpa.GHJ.donjon.entites.Salle;
import fr.afpa.GHJ.donjon.entites.SortieDonjon;
import fr.afpa.GHJ.donjon.services.ServiceDonjon;
import junit.framework.TestCase;

public class TestServiceDonjon extends TestCase {
	
	@Test
	public void testAjoutMonstreOuPas() {
		List<Monstre> monstre = new ArrayList<Monstre>();
		Monstre monstreTest = new Monstre(0, 0, 0);
		Monstre monstreAleaTest = new MonstreAlea(0, 0, 0);
		Monstre monstreUneFoisSurDeuxTest = new MonstreUneFoisSurDeux(0, 0, 0);
		
		
		

	
		
		
	}

	@Test
	public void testConvertirSalle() {

		ServiceDonjon testDonjon = new ServiceDonjon(1, 1);
		

		assertEquals(testDonjon.convertirSalle('a'), new Salle(null, true, false, false, false));

		assertEquals(testDonjon.convertirSalle('b'), new Salle(null, false, false, true, false));

		assertEquals(testDonjon.convertirSalle('c'), new Salle(null, false, true, false, false));

		assertEquals(testDonjon.convertirSalle('d'), new Salle(null, false, false, false, true));

		assertEquals(testDonjon.convertirSalle('e'), new Salle(null, true, false, true, false));

		assertEquals(testDonjon.convertirSalle('f'), new Salle(null, false, true, true, false));

		assertEquals(testDonjon.convertirSalle('g'), new Salle(null, false, true, false, true));

		assertEquals(testDonjon.convertirSalle('h'), new Salle(null, true, false, false, true));

		assertEquals(testDonjon.convertirSalle('i'), new Salle(null, true, false, true, true));

		assertEquals(testDonjon.convertirSalle('j'), new Salle(null, true, true, true, false));

		assertEquals(testDonjon.convertirSalle('k'), new Salle(null, false, true, true, true));

		assertEquals(testDonjon.convertirSalle('l'), new Salle(null, true, true, false, true));

		assertEquals(testDonjon.convertirSalle('m'), new Salle(null, true, true, false, false));

		assertEquals(testDonjon.convertirSalle('n'), new Salle(null, false, false, true, true));

		assertEquals(testDonjon.convertirSalle('o'), new Salle(null, true, true, true, true));
		
		assertEquals(testDonjon.convertirSalle('p'), new EntreeDonjon(null, false, true, false, false));
		
		assertEquals(testDonjon.convertirSalle('q'), new EntreeDonjon(null,false, false, true, false));
		
		assertEquals(testDonjon.convertirSalle('r'), new EntreeDonjon(null, false, true, true, false));
		
		assertEquals(testDonjon.convertirSalle('s'), new SortieDonjon(null, true, false, false, false));
		
		assertEquals(testDonjon.convertirSalle('t'), new SortieDonjon(null, false, true, false, false));
		
		assertEquals(testDonjon.convertirSalle('u'), new SortieDonjon(null, false, false, true, false));
		
		assertEquals(testDonjon.convertirSalle('v'), new SortieDonjon(null,false, false, false, true));
		
		assertEquals(testDonjon.convertirSalle('w'), null);

	}

	@Test
	public void testRechercheSalleFinale() {
		
		Donjon donjonTest = new Donjon(2,2);
		
		Salle salle1 = new Salle(null, false, false, true, true);
		Salle salle2 = new Salle(null, false, true, false, true);
		Salle salle3 = new Salle(null, true, false, false, false);
		Salle salle4 = new SortieDonjon(null, true, false, false, false);
		
		donjonTest.setOneSalle(salle1, 0, 0);
		donjonTest.setOneSalle(salle2, 0, 1);
		donjonTest.setOneSalle(salle3, 1, 0);
		donjonTest.setOneSalle(salle4, 1, 1);
		
		assertTrue(salle4 == ServiceDonjon.rechercheSalleFinale(donjonTest));
			
	}
	
	@Test
	public void testRechercheSalleViaJoueur() {
		
		Donjon donjonTest = new Donjon(2,2);
		
		
		Joueur joueurTest = new Joueur(100, 20, 0, "testeur");
		
		Salle salle1 = new Salle(null, false, false, true, true);
		Salle salle2 = new Salle(null, false, true, false, true);
		Salle salle3 = new Salle(null, true, false, false, false);
		Salle salle4 = new Salle(null, true, false, false, false);
		
		donjonTest.setOneSalle(salle1, 0, 0);
		donjonTest.setOneSalle(salle2, 0, 1);
		donjonTest.setOneSalle(salle3, 1, 0);
		donjonTest.setOneSalle(salle4, 1, 1);
		
		assertEquals(null, ServiceDonjon.rechercheSalleViaJoueur(donjonTest, joueurTest));
		
		salle1.setJoueur(joueurTest);
		
		assertTrue(salle1 == ServiceDonjon.rechercheSalleViaJoueur(donjonTest, joueurTest));
		
		}
	
	@Test
	public void testRechercheSalleviaMonstre() {
		
		Donjon donjonTest = new Donjon(2,2);
		
		Monstre monstreTest = new Monstre(0, 0, 0);
		
		Salle salle1 = new Salle(null, false, false, true, true);
		Salle salle2 = new Salle(null, false, true, false, true);
		Salle salle3 = new Salle(null, true, false, false, false);
		Salle salle4 = new Salle(null, true, false, false, false);
		
		donjonTest.setOneSalle(salle1, 0, 0);
		donjonTest.setOneSalle(salle2, 0, 1);
		donjonTest.setOneSalle(salle3, 1, 0);
		donjonTest.setOneSalle(salle4, 1, 1);
		
assertEquals(null, ServiceDonjon.rechercheSalleViaMonstre(donjonTest, monstreTest));
		
		salle1.getListeDeMonstre().add(monstreTest);
		
		assertTrue(salle1 == ServiceDonjon.rechercheSalleViaMonstre(donjonTest, monstreTest));
		
		
		
	}
	
	
	
	@Test
	public void testMethodeChange() {
		
		
		assertEquals('a', ServiceDonjon.change(1, false, false));
		assertEquals('c', ServiceDonjon.change(2, false, false));
		assertEquals('m', ServiceDonjon.change(3, false, false));
		assertEquals('b', ServiceDonjon.change(4, false, false));
		assertEquals('e', ServiceDonjon.change(5, false, false));
		assertEquals('f', ServiceDonjon.change(6, false, false));
		assertEquals('j', ServiceDonjon.change(7, false, false));
		assertEquals('d', ServiceDonjon.change(8, false, false));
		assertEquals('h', ServiceDonjon.change(9, false, false));
		assertEquals('g', ServiceDonjon.change(10, false, false));
		assertEquals('l', ServiceDonjon.change(11, false, false));
		assertEquals('n', ServiceDonjon.change(12, false, false));
		assertEquals('i', ServiceDonjon.change(13, false, false));
		assertEquals('k', ServiceDonjon.change(14, false, false));
	    assertEquals('o', ServiceDonjon.change(15, false, false));
		
		assertEquals('p', ServiceDonjon.change(2, true, false));
		assertEquals('q', ServiceDonjon.change(4, true, false));
		assertEquals('r', ServiceDonjon.change(6, true, false));
		assertEquals('m', ServiceDonjon.change(3, true, false));
		
		assertEquals('s', ServiceDonjon.change(97, false, true));
		assertEquals('u', ServiceDonjon.change(98, false, true));
		assertEquals('t', ServiceDonjon.change(99, false, true));
		assertEquals('v', ServiceDonjon.change(100, false, true));
		assertEquals('m', ServiceDonjon.change(3, false, true));
		
		assertEquals('0', ServiceDonjon.change(16, false, false));
		
		
	}
	
@Test

public void testDecimateHorinzontally() {
	
	
	
}
	
}
