package fr.afpa.GHJ.donjon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Scanner;

import org.junit.Test;

import fr.afpa.GHJ.donjon.entites.Donjon;
import fr.afpa.GHJ.donjon.entites.Joueur;
import fr.afpa.GHJ.donjon.entites.Salle;
import fr.afpa.GHJ.donjon.services.ServiceSalle;

public class TestServiceSalle {

	@Test
	public void testDeplacementPersonnageOK() {
		Donjon donjon = new Donjon(2, 2);
		Joueur joueur = new Joueur("a");
		Salle salle1 = new Salle(joueur, true, true, true, true);
		Salle salle2 = new Salle(null, true, true, true, true);
		Salle salle3 = new Salle(null, true, true, true, true);
		Salle salle4 = new Salle(null, true, true, true, true);
		donjon.setOneSalle(salle1, 0, 0);
		donjon.setOneSalle(salle2, 1, 0);
		donjon.setOneSalle(salle3, 1, 1);
		donjon.setOneSalle(salle4, 0, 1);
		Scanner s = new Scanner("s\ne\nn\no\n");
		assertTrue(ServiceSalle.DeplacementPersonnage(salle1, joueur, donjon, s));
		assertTrue(ServiceSalle.DeplacementPersonnage(salle2, joueur, donjon, s));
		assertTrue(ServiceSalle.DeplacementPersonnage(salle3, joueur, donjon, s));
		assertTrue(ServiceSalle.DeplacementPersonnage(salle4, joueur, donjon, s));
		s.close();
	}
	
	@Test
	public void testDeplacementPersonnageKOException() {
		Donjon donjon = new Donjon(2, 2);
		Joueur joueur = new Joueur("a");
		Salle salle1 = new Salle(joueur, false, false, false, false);
		donjon.setOneSalle(salle1, 0, 0);
		Scanner s = new Scanner("\n");
		assertFalse(ServiceSalle.DeplacementPersonnage(salle1, joueur, donjon, s));
		s.close();
	}
	
	@Test
	public void testDeplacementPersonnageKO() {
		Donjon donjon = new Donjon(2, 2);
		Joueur joueur = new Joueur("a");
		Salle salle1 = new Salle(joueur, false, false, false, false);
		Salle salle2 = new Salle(joueur, false, false, false, false);
		Salle salle3 = new Salle(joueur, false, false, false, false);
		Salle salle4 = new Salle(joueur, false, false, false, false);
		donjon.setOneSalle(salle1, 0, 0);
		donjon.setOneSalle(salle2, 1, 0);
		donjon.setOneSalle(salle3, 1, 1);
		donjon.setOneSalle(salle4, 0, 1);
		Scanner s1 = new Scanner("s\ne\nn\no\n");
		Scanner s2 = new Scanner("s\ne\nn\no\n");
		Scanner s3 = new Scanner("s\ne\nn\no\n");
		Scanner s4 = new Scanner("s\ne\nn\no\na\n");
		assertFalse(ServiceSalle.DeplacementPersonnage(salle1, joueur, donjon, s1));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle1, joueur, donjon, s1));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle1, joueur, donjon, s1));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle1, joueur, donjon, s1));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle2, joueur, donjon, s2));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle2, joueur, donjon, s2));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle2, joueur, donjon, s2));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle2, joueur, donjon, s2));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle3, joueur, donjon, s3));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle3, joueur, donjon, s3));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle3, joueur, donjon, s3));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle3, joueur, donjon, s3));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle4, joueur, donjon, s4));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle4, joueur, donjon, s4));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle4, joueur, donjon, s4));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle4, joueur, donjon, s4));
		assertFalse(ServiceSalle.DeplacementPersonnage(salle4, joueur, donjon, s4));
		s1.close();
		s2.close();
		s3.close();
		s4.close();
	}
	
	@Test
	public void testScanSalle() {
		
		Donjon donjonTest = new Donjon(2,2);
		Joueur joueurTest = new Joueur(100, 20, 0, "testeur");
		Salle salle1 = new Salle(joueurTest, true, true, true, true);
		Salle salle2 = new Salle(joueurTest, true, true, true, true);
		Salle salle3 = new Salle(joueurTest, true, true, true, true);
		Salle salle4 = new Salle(joueurTest, true, true, true, true);	
		
		donjonTest.setOneSalle(salle1, 0, 0);
		donjonTest.setOneSalle(salle2, 0, 1);
		donjonTest.setOneSalle(salle3, 1, 1);
		donjonTest.setOneSalle(salle4, 1, 0);
		
	assertEquals(ServiceSalle.scanSalle(salle3, donjonTest, joueurTest, 'N'), donjonTest.getOneSalle(1 - 1, 1));
	assertEquals(ServiceSalle.scanSalle(salle1, donjonTest, joueurTest, 'S'), donjonTest.getOneSalle(0 + 1, 0));
		assertEquals(ServiceSalle.scanSalle(salle1, donjonTest, joueurTest, 'E'), donjonTest.getOneSalle(0, 0 + 1));
		assertEquals(ServiceSalle.scanSalle(salle2, donjonTest, joueurTest, 'O'), donjonTest.getOneSalle(0, 1-1));
		assertEquals(ServiceSalle.scanSalle(salle2, donjonTest, joueurTest, 'F'), null);
		

		
		
	}

}
